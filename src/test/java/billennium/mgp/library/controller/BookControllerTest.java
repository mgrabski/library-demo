package billennium.mgp.library.controller;

import billennium.mgp.library.model.Book;
import billennium.mgp.library.service.BookService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.StreamUtils;

import java.nio.charset.Charset;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class BookControllerTest {

    public static final Book SAVED_BOOK = new Book(UUID.fromString("7dbb3ccf-9c74-488f-bf5d-39ef62988e2c"),
            "Oni",
            "Teresa Toranska",
            "9783241987222");

    @MockBean
    private BookService bookService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private BookController bookController;

    @Value("classpath:testdata/bookRequest.json")
    private Resource bookRequest;

    @Value("classpath:testdata/bookResponse.json")
    private Resource bookResponse;

    @Test
    @SneakyThrows
    public void createBook_ValidBookRequestAsParam_ReturnsValidOkResponse(){
        Mockito.when(bookService.createBook(Mockito.any())).thenReturn(SAVED_BOOK);
        mvc.perform(MockMvcRequestBuilders
                .post("/book")
                .content(convertResourceToString(bookRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(convertResourceToString(bookResponse)));
    }

    @SneakyThrows
    private String convertResourceToString(Resource resource) {
        return StreamUtils.copyToString(resource.getInputStream(), Charset.defaultCharset());
    }

}
