package billennium.mgp.library.problem;

import billennium.mgp.library.model.Book;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class InvalidISBNException extends CustomException {

    public static final String ERROR_CODE = "001";
    public static final String ERROR_MESSAGE = "Invalid ISBN number provided for book: %s";

    public InvalidISBNException(Book book) {
        super(String.format(ERROR_MESSAGE, book.toString()));
        this.errorCode = ERROR_CODE;
        this.objects = Map.of("book", book);
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }

}
