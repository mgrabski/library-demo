package billennium.mgp.library.problem;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Getter
public class CustomExceptionResponse {

    private String errorCode;
    private String message;
    private Map<Object, Object> objects;
    private HttpStatus httpStatus;

    public CustomExceptionResponse(CustomException customException){
        this.errorCode = customException.getErrorCode();
        this.message = customException.getMessage();
        this.objects = customException.getObjects();
        this.httpStatus = customException.getHttpStatus();
    }

    public CustomExceptionResponse(RuntimeException runtimeException){
        this.errorCode = "500";
        this.message = runtimeException.getMessage();
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

}
