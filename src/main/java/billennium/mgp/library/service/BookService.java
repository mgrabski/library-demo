package billennium.mgp.library.service;

import billennium.mgp.library.model.Book;

public interface BookService {

    public Book createBook(Book book);

}
