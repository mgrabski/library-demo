package billennium.mgp.library.service;

import billennium.mgp.library.model.Book;
import billennium.mgp.library.repository.BookRepository;
import billennium.mgp.library.problem.InvalidISBNException;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    public static final String ISBN_REGEX = "[0-9]+";

    private BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book createBook(Book book) {
        validateISBN(book);
        return bookRepository.save(book);
    }

    private void validateISBN(Book book) {
        String isbn = book.getIsbn();
        if (isbn.length() != 13 || !isbn.matches(ISBN_REGEX)) {
            throw new InvalidISBNException(book);
        }
    }
}
